/*
 * (C) 2021 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of CC0-1.0 license
 */


using System;
using System.IO;
using System.Diagnostics;


public class CustomCakeHelpers
{
    /** <summary>
        Helper to find "bin" and "obj" directories.
        </summary> */
    public static List<string> FindBuilt(string [] directoryPathList) {
        var dirsRet = new List<string>();
        foreach (string directoryPath in
                 directoryPathList) {
            foreach (string l1Dir in
                     System.IO.Directory.GetDirectories(directoryPath)) {
                foreach (string l2Dir in
                         System.IO.Directory.GetDirectories(l1Dir)) {
                    if (l2Dir.Contains("/bin") || l2Dir.Contains("/obj")) {
                        dirsRet.Add(l2Dir);
                    }
                }
            }
        }
        return dirsRet;
    }
}


var target = Argument("target", "Build");
var config = Argument("configuration", "Release");
var slnDir = "./";
var outDir = "./artifacts";
var binDir = outDir + "/bin";
var docDir = outDir + "/doc";


Task("Clean")
    .Does(() => {
        foreach (string dir in
                 CustomCakeHelpers.FindBuilt(new string [] {"src", "tests"})) {
            CleanDirectory(dir);
        }
        CleanDirectory(outDir);
    });

Task("Restore")
    .Does(() => {
        DotNetRestore(slnDir);
    });

Task("Build")
    .IsDependentOn("Restore")
    .Does(() => {
        DotNetBuild(slnDir, new DotNetBuildSettings
        {
            Configuration = config,
            NoRestore = true
        });
    });

Task("Documentation")
    .Does(() => {
        ProcessStartInfo startInfo = new ProcessStartInfo() {
            FileName = "doxygen", Arguments = "docs/doxygen.config",
        };
        Process proc = new Process() { StartInfo = startInfo, };
        proc.Start();
        proc.WaitForExit();
    });

Task("Test")
    .Does(() => {
        DotNetTest(slnDir, new DotNetTestSettings
        {
            Configuration = config,
            NoBuild = true,
            NoRestore = true
        });
    });

Task("Publish")
    .IsDependentOn("Clean")
    .IsDependentOn("Build")
    .IsDependentOn("Documentation")
    .Does(() => {
        DotNetPublish(slnDir, new DotNetPublishSettings
        {
            Configuration = config,
            NoBuild = true,
            NoRestore = true,
            OutputDirectory = binDir
        });
    });


RunTarget(target);
