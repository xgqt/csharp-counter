/*
 * (C) 2021 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of CC0-1.0 license
 */


namespace Counter
{
    public class Counter : Meter
    {
        public Counter(float metervalue)
            : base(0, 1, (float a, float b) => { return a + b; })
        {
            this.meterValue = metervalue;
        }
    }
}
