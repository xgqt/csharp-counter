/*
 * (C) 2021 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of CC0-1.0 license
 */


namespace Counter
{
    public class Meter
    {
        public delegate float GrowFunction(float val, float ivl);


        public float meterValue { get; set; }
        public float meterInterval { get; set; }
        public GrowFunction meterGrowFunction { get; set; }


        public Meter(float metervalue, float meterinterval,
                     GrowFunction metergrowfunction)
        {
            this.meterValue = metervalue;
            this.meterInterval = meterinterval;
            this.meterGrowFunction = metergrowfunction;
        }


        /** <summary>
            Given a single argument times (int) the method executes
            the meterGrowFunction given number of times.
            </summary> */
        public void Grow(int times)
        {
            for (int i = 0; i < times; i++)
            {
                this.meterValue =
                    this.meterGrowFunction(this.meterValue, this.meterInterval);
            }
        }

        /** <summary>
            Given a single argument times (int) the method calls
            the Grow method and returns the meterValue (float).
            </summary> */
        public float Runs(int times)
        {
            Grow(times);
            return this.meterValue;
        }

        /** <summary>
            The method returns the result of
            the Runs method call with a argument equal to 1.
            </summary> */
        public float Run()
        {
            return Runs(1);
        }
    }
}
