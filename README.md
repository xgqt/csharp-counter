# C# Counter

This is a rewrite of one of my scheme projects - 
[scheme-counter](https://gitlab.com/xgqt/scheme-counter)
to experiment with .NET


# License

Public Domain or CC0-1.0, whichever your country accepts.
