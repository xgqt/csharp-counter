/*
 * (C) 2021 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of CC0-1.0 license
 */


using Xunit;


namespace Counter.UnitTests
{
    public class MeterTest
    {
        [Fact]
        public void Subtraction() {
            Meter mtr = new Meter(100, 1,
                                  (float a, float b) => {return a - b;});
            Assert.Equal(0, mtr.Runs(100));
        }

        [Fact]
        public void Multiplication() {
            Meter mtr = new Meter(2, 2,
                                  (float a, float b) => {return a * b;});
            Assert.Equal(2048, mtr.Runs(10));
        }

        [Fact]
        public void Division() {
            Meter mtr = new Meter(200, 2,
                                  (float a, float b) => {return a / b;});
            Assert.Equal(50, mtr.Runs(2));
        }

        public float ValuePlusDoubleInterval(float a, float b) {
            return a + b*2;
        }
        [Fact]
        public void CustomFunction() {
            Meter mtr = new Meter(2, 3, ValuePlusDoubleInterval);
            Assert.Equal(8, mtr.Run());
        }
    }
}
