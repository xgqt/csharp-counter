/*
 * (C) 2021 Maciej Barć <xgqt@riseup.net>
 * Licensed under the terms of CC0-1.0 license
 */


using Xunit;


namespace Counter.UnitTests
{
    public class CounterTest
    {
        [Fact]
        public void Grow() {
            for (int i = 0; i < 100; i++) {
                Counter cntr = new Counter(0);
                cntr.Grow(i);
                Assert.Equal(i, cntr.meterValue);
            }
        }

        [Fact]
        public void Run() {
            for (int i = 0; i < 100; i++) {
                Counter cntr = new Counter(i - 1);
                Assert.Equal(i, cntr.Run());
            }
        }

        [Fact]
        public void Runs() {
            for (int i = 0; i < 100; i++) {
                Counter cntr = new Counter(0);
                Assert.Equal(i, cntr.Runs(i));
            }
        }
    }
}
